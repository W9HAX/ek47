BANNER = """ 
     ⢈⣸⣽⣿⣟⢏⢈                               
   ⣀⣌⣯⡷⡷⡷⡷⣷⣾⡌⠌                             
 ⠠⣮⣿⣿⠟     ⣿⣿⣯⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⠢⠢⠢     ⣠⣪⣮⢮⠎   
 ⢀⣿⣿⣿⠏     ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣌⣌⣌⣌⣿⣿⣏⣌⣼⣿⣿⣿⣏⣌  
   ⡴⡵⣿⣮⣮⣮⣮⣮⣿⡷⡷⡷⣷⣿⣿                         
    ⠣⣳⣷⣿⡿⠿⠲   ⠰⠲⡿⣟⢏⢈  Ek47 ({}) - Environmental
      ⠐⠑⠑⠑⠁      ⠑⣱⣻⣺  Keying for .NET/native PEs
""" # ASCII art (required by TrustedSec)

EPILOG = """\
description:
    Encrypt a payload of format: {} 
    With several supported environmental keys: {} 
    To many supported output formats: {}
"""