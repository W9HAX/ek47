import re
import os
import sys
import random

def get_line_from_string(string: str, code: str) -> str:
    # Returns the whole line that the string is found on
    index = code.find(string)
    if index == -1:
        raise ValueError("Obfuscate string extracted from C# code was not found in C# code. Developer error occured :(")
    else:
        newline_start = code.rfind("\n", 0, index)
        newline_end =   code.find("\n", index+len(string))
        return code[newline_start+1:newline_end]

def should_obfuscate(line: str, string: str) -> bool:
    # Given a line and string of code, determine if we should obfuscate it.
    # There will be a handful of exceptions to string obfuscation
    # such as constant strings or attribute strings used in DllImport
    except_line_strings = [   # Things that should not be on the same line as a string
                              ')]', #DllImport
                              'const ',
                          ]

    except_string_strings = [   # Things that should not end up in the string itself
                                '\\x'
                            ]

    for except_line_string in except_line_strings:
        if except_line_string in line:
            return False

    for except_string_string in except_string_strings:
        if except_string_string in line:
            return False
    return True

def find_suitable_replace_char(string: str) -> chr:
    # Find a char that is not in the string we can use to pad the string
    string = string.strip('"')

    pad_char_list = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=|")
    random.shuffle(pad_char_list)
    chars = ''.join(pad_char_list)

    for char in chars:
        if char not in string:
            return char
    return "" # No suitable character found for padding. We will not obfuscate this string

def obfuscate_string_pad_with_chars(string: str, pad: str) -> str:
    # Given a string, pad with chars and generate a suitable replacement
    result = ""
    string = string.strip('"')
    inside_brackets = False

    for char in string:
        if char == "\\" or char == "{" or char == "}" or inside_brackets:
            result += char
            if char == "{":
                inside_brackets = True
            if char == "}":
                inside_brackets = False
        else:
            result += char + pad

    replacer = f'.Replace("{pad}", "")'
    return f'"{result}"{replacer}'

def obfuscate_cs_code(code: str) -> str:
    # Given C# source code, obfuscate it
    # Currently only supports string obfuscation
    string_pattern = re.compile(r'"(?:\\.|[^"\\])*"', re.DOTALL)
    matches = string_pattern.findall(code)

    for string in matches:
        if(should_obfuscate(get_line_from_string(string, code), string)):
            pad = find_suitable_replace_char(string)
            if pad != "":
                obf_string = obfuscate_string_pad_with_chars(string, pad)
                code = code.replace(string, obf_string, 1) 
    return code

def __main__run_as_script():
    if len(sys.argv) > 1:
        file_path = sys.argv[1]
    
        if os.path.exists(file_path):
            with open(file_path, 'r') as fd:
                csharp_code = fd.read()
        else:
            print(f"Error: File '{file_path}' does not exist.")
            quit()
    else:
        print("Error: No command line arguments provided. Give a path to a C# file you wish to string obfuscate.")
        quit()

    print(obfuscate_cs_code(csharp_code))

if __name__ == "__main__":
    __main__run_as_script()
