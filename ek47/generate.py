import os
import random
import struct
import hashlib

from Crypto.PublicKey import RSA
from Crypto.Util.number import long_to_bytes
from Crypto.Hash import SHA
from io import BytesIO

alphabet = []
business_words = []

with open(f'{os.getcwd()}/resources/alphabet.txt', 'r') as _alphabet:
    for character in _alphabet:
        alphabet.append(character.rstrip())

with open(f'{os.getcwd()}/resources/business_words.txt', 'r') as _business_words:
    for word in _business_words:
        business_words.append(word.rstrip())


def generate_random_business_words(min_length: int = 5, max_length: int = 60) -> str:
    """
    Generate a random sentence from the given word list with a specified length.

    Args:
        min_length (int, optional): Minimum length of the sentence (number of words). Defaults to 5.
        max_length (int, optional): Maximum length of the sentence (number of words). Defaults to 60.

    Returns:
        str: A random sentence.
    """
    sentence_length = random.randint(min_length, max_length)
    sentence = " ".join(random.choices(business_words, k=sentence_length))
    sentence = sentence.capitalize() + "."
    return sentence

def pick_random_business_word() -> str:
    return random.choice(business_words)

def generate_random_hex_string(length: int = 10) -> str:
    hex_characters = '0123456789abcdef'
    hex_string = ''.join(random.choice(hex_characters) for _ in range(length))
    return hex_string


def generate_string_identifier(length: int = 10) -> str:
    """
    Generate random upper and lowercase characters and concatenate
    them for a length of zero to *length*.
    :param: int length: The amount of random characters to concatenate.
    :return: The generated string identifier.
    :rtype: str
    """
    _identifier = [alphabet[random.randint(0, len(alphabet) - 1)].rstrip() for _ in range(0, length)]
    _identifier = ''.join(_identifier)
    return _identifier

def generate_snk_file(filename: str, key_size: int = 1024) -> str:
    # Generates a self-signed Strong Name Key file which can be used with
    # the `mcs` compiler's -keyfile: flag to sign an assembly with a "strong name".
    # Equivalent to the `sn -k MyKey.snk` command.
    # Signing an assembly is required for regsvcs.exe and regasm.exe AWL bypasses.
    # Reference: https://github.com/AppCoreNet/SigningTool/blob/dac2e64c2913e79bc96016a3befe1891be1f21fa/src/AppCore.SigningTool/StrongName/StrongNameKeyGenerator.cs#L19-L41
    # Returns the public key token for the generated SNK file

    rsa = RSA.generate(key_size)
    RSA2_SIG = 0x32415352

    parameters = rsa.export_key()
    e = long_to_bytes(rsa.e)
    n = long_to_bytes(rsa.n)
    p = long_to_bytes(rsa.p)
    q = long_to_bytes(rsa.q)
    d = long_to_bytes(rsa.d)
    dp = long_to_bytes(rsa.d % (rsa.p-1))
    dq = long_to_bytes(rsa.d % (rsa.q-1))
    qInv = long_to_bytes(pow(rsa.q, rsa.p-2, rsa.p))

    out_stream = BytesIO()
    writer = BytesIO()
    writer.write(struct.pack('B', 7))  # bType (public/private key)
    writer.write(struct.pack('B', 2))  # bVersion
    writer.write(struct.pack('H', 0))  # reserved
    writer.write(struct.pack('I', 0x00002400))  # aiKeyAlg (CALG_RSA_SIGN)
    writer.write(struct.pack('I', RSA2_SIG))  # magic (RSA2)
    writer.write(struct.pack('I', len(n)*8))  # bitlen
    writer.write(e[::-1])  # exponent
    writer.write(b'\x00' * (4 - len(e) % 4))  # pad to DWORD
    writer.write(n[::-1])  # modulus
    writer.write(p[::-1])  # prime1
    writer.write(q[::-1])  # prime2
    writer.write(dp[::-1])  # exponent1
    writer.write(dq[::-1])  # exponent2
    writer.write(qInv[::-1])  # coefficient
    writer.write(d[::-1])  # privateExponent
    out_stream.write(writer.getvalue())

    snk_data = out_stream.getvalue()
    with open(filename, "wb") as f:
        f.write(snk_data)

    # Calculate and return the public key and public key token from the rsa object
    n = rsa.n.to_bytes((rsa.n.bit_length() + 7) // 8, 'big')
    e = rsa.e.to_bytes((rsa.e.bit_length() + 7) // 8, 'big')
    pub_key = create_public_key(n, e)
    pub_key_token = create_public_key_token(pub_key)
    return pub_key_token

def create_public_key(modulus, public_exponent, sig_alg = 0x2400, hash_alg = 0x8004):
    # From an RSA keypair, generate the appropriate representation of the public key,
    # such that it can be used to calculate the public key token
    # sig_alg default (0x2400) = SignatureAlgorithm.CALG_RSA_SIGN
    # hash_alg default (0x8004) = AssemblyHashAlgorithm.SHA1
    stream = BytesIO()
    stream.write(struct.pack('<I', sig_alg))
    stream.write(struct.pack('<I', hash_alg))
    stream.write(struct.pack('<I', 20 + len(modulus)))
    stream.write(struct.pack('BBH', 6, 2, 0))
    stream.write(struct.pack('<I', sig_alg))
    stream.write(struct.pack('<I', 826364754))
    stream.write(struct.pack('<I', len(modulus) * 8))
    stream.write(public_exponent[::-1])  # Reverse the byte order
    stream.write(struct.pack('s', b'\x00')) # Not sure if this is good, but this was required to pack these bytes from 318 length to 320 (hex chars)
    stream.write(modulus[::-1])  # Reverse the byte order

    return stream.getvalue()

def create_public_key_token(public_key_data) -> str:
    # From a properly formatted public key, generate the public key token
    hash_algorithm = 0x8004  # Equivalent to AssemblyHashAlgorithm.SHA1 in C#
    hashed_data = hashlib.sha1(public_key_data).digest()

    token_bytes = bytearray([0] * 8)
    for i in range(min(8, len(hashed_data))):
        token_bytes[i] = hashed_data[-(i + 1)]

    return bytes(token_bytes).hex().lower()
