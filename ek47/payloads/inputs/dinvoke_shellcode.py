from . import input


class DinvokeShellcode(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('dinvoke-shellcode.cs.template', 'dinvoke-shellcode', 'Embed shellcode into an Ek47 payload. Uses DInvoke and manually maps NTDLL to execute NT functions.')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py dinvoke-shellcode -c SRV_SCCM -p shellcode.bin -o test.exe']

    def render_template(self, arguments, input_payload, template):
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
        )
        return template
